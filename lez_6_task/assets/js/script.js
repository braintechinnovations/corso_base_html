function inserisciLibro(){
    var varIsbn = document.getElementById("varIsbn");
    var varTitolo = document.getElementById("varTitolo");
    var varAutore = document.getElementById("varAutore");
    var varCategoria = document.getElementById("varCategoria");

    if(!varIsbn.value || !varTitolo.value || !varAutore.value){
        alert("Ciccio, controlla i campi! ;)");
        return;
    }    

    var objLibro = {
        isbn: varIsbn.value,
        titolo: varTitolo.value,
        autore: varAutore.value,
        categoria: varCategoria.value
    }

    varIsbn.value = "";
    varTitolo.value = "";
    varAutore.value = "";
    varCategoria.value = "";

    // document.getElementById("varIsbn").value = "";
    // document.getElementById("varTitolo").value = "";
    // document.getElementById("varAutore").value = "";
    // document.getElementById("varCategoria").value = "";

    elencoLibri.push(objLibro);
    stampaLibri(elencoLibri)
}

function stampaLibri(elencoLibriDaStampare){

    var stringaHtml = "";

    for(var i=0; i<elencoLibriDaStampare.length; i++){
        var temp = elencoLibriDaStampare[i];

        stringaHtml += "<tr>";
        stringaHtml +=      "<td>" + temp.isbn + "</td>";
        stringaHtml +=      "<td>" + temp.titolo + "</td>";
        stringaHtml +=      "<td>" + temp.autore + "</td>";
        stringaHtml +=      "<td>" + temp.categoria + "</td>";
        stringaHtml += "</tr>";
    }

    document.getElementById("contenuto-tabella").innerHTML = stringaHtml;

}

function ricercaLibro(){
    var autoreRicerca = document.getElementById("varRicercaAutore").value;

    var elencoLibriRicercati = [];

    for(var i=0; i<elencoLibri.length; i++){
        var temp = elencoLibri[i];

        if(temp.autore == autoreRicerca){
            elencoLibriRicercati.push(temp);
        }
    }

    stampaLibri(elencoLibriRicercati);

}

var elencoLibri = [];