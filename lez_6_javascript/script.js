function inserisciPersona(){
    var varNome = document.getElementById("varNome").value;
    var varCognome = document.getElementById("varCognome").value;
    var varNascita = document.getElementById("varNascita").value;
    var varAuto = document.getElementById("varAuto").value;

    if(varNome.length == 0 || varCognome.length == 0){
        alert("Attenzione ai campi obbligatori");
        return;
    }

    //Creo il nuovo oggetto
    var varOggPersona = {
        nome: varNome,
        cognome: varCognome,
        nascita: varNascita,
        auto: varAuto
    }

    //Aggiungo il novo oggetto all'elenco
    elencoPersone.push(varOggPersona);

    //Pulizia dei campi
    document.getElementById("varNome").value = "";
    document.getElementById("varCognome").value = "";
    document.getElementById("varNascita").value = "";
    document.getElementById("varAuto").value = "";

    stampaElencoPersona();
}

function stampaElencoPersona(){

    var stringaRisultante = "";

    for(var i=0; i<elencoPersone.length; i++){
        var temp = elencoPersone[i];

        stringaRisultante += "<tr>";
        stringaRisultante +=    "<td>" + temp.nome + "</td>";   
        stringaRisultante +=    "<td>" + temp.cognome + "</td>";   
        stringaRisultante +=    "<td>" + temp.nascita + "</td>";    
        stringaRisultante +=    "<td>" + temp.auto + "</td>";  
        stringaRisultante += "</tr>";
    }

    document.getElementById("corpo-tabella").innerHTML = stringaRisultante;
}

//MAIN - Scope Globale
var elencoPersone = [];

/**
 * Creare un sistema di gestione libri:
 * Ogni libro è caratterizzato da:
 * - ISBN
 * - Titolo
 * - Autore
 * - Categoria (una)
 * 
 * Popolare una tabella con tutti i libri appena inseriti!
 * 
 * HARD: Creare un input di ricerca che, dato in input l'autore, popoli la tabella
 * con tutti i libri relativi a quell'autore! (filtra i dati già presenti in precedenza)
 */