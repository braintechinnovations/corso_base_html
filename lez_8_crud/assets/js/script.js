function inserisciPersona(){

    var varNome = document.getElementById("varNome").value;
    var varEmail = document.getElementById("varEmail").value;
    var varCognome = document.getElementById("varCognome").value;
    var varSesso = document.getElementById("varSesso").value;

    var objPersona = {
        id: contatore++,
        nome: varNome,
        email: varEmail,
        cognome: varCognome,
        sesso: varSesso
    }

    // document.getElementById("varNome").value = "";
    // document.getElementById("varEmail").value = "";
    // document.getElementById("varCognome").value = "";
    // document.getElementById("varSesso").value = "F";    

    elencoPersone.push(objPersona);
    stampaPersone(elencoPersone)
}

function stampaPersone(arrayDaStampare){
    var stringaHtml = "";

    for(var i=0; i<arrayDaStampare.length; i++){
        var temp = arrayDaStampare[i];

        stringaHtml += "<tr>";
        stringaHtml +=      "<td>" + temp.nome + "</td>";
        stringaHtml +=      "<td>" + temp.cognome + "</td>";
        stringaHtml +=      "<td>" + temp.email + "</td>";
        stringaHtml +=      "<td>" + temp.sesso + "</td>";
        stringaHtml +=      "<td>";
        stringaHtml +=      "   <button type=\"button\" class=\"btn btn-danger\" onclick=\"eliminaPersona(" + temp.id + ")\">";
        stringaHtml +=      "       <i class=\"fas fa-trash-alt\"></i>";
        stringaHtml +=      "   </button>";
        stringaHtml +=      "   <button type=\"button\" class=\"btn btn-warning\" onclick=\"modificaPersona(" + temp.id + ")\">";
        stringaHtml +=      "       <i class=\"fas fa-edit\"></i>";
        stringaHtml +=      "   </button>";
        stringaHtml +=      "</td>";
        stringaHtml += "</tr>";
    }

    document.getElementById("contenuto-tabella").innerHTML = stringaHtml;
}

function eliminaPersona(varIdentificatore){

    for(var i=0; i<elencoPersone.length; i++){
        var temp = elencoPersone[i];

        if(temp.id == varIdentificatore){
            elencoPersone.splice(i, 1);
        }
    }

    stampaPersone(elencoPersone);

}

function modificaPersona(varIdentificatore){
    for(var i=0; i<elencoPersone.length; i++){
        var temp = elencoPersone[i];

        if(temp.id == varIdentificatore){
            document.getElementById("varModId").value = temp.id;
            document.getElementById("varModNome").value = temp.nome;
            document.getElementById("varModCognome").value = temp.cognome;
            document.getElementById("varModEmail").value = temp.email;
            document.getElementById("varModSesso").value = temp.sesso;
        }
    }

    $("#modaleModifica").modal('show');
}

function effettuaModifica(){
    var varModId = document.getElementById("varModId").value;
    var varModNome = document.getElementById("varModNome").value;
    var varModCognome = document.getElementById("varModCognome").value;
    var varModEmail = document.getElementById("varModEmail").value;
    var varModSesso = document.getElementById("varModSesso").value;

    for(var i=0; i<elencoPersone.length; i++){
        var temp = elencoPersone[i];

        if(temp.id == varModId){
            temp.nome = varModNome;
            temp.cognome = varModCognome;
            temp.email = varModEmail;
            temp.sesso = varModSesso;
        }
    }

    stampaPersone(elencoPersone);

    document.getElementById("varModId").value = "";
    document.getElementById("varModNome").value = "";
    document.getElementById("varModCognome").value = "";
    document.getElementById("varModEmail").value = "";
    document.getElementById("varModSesso").value = "";

    $("#modaleModifica").modal('hide');
}

var contatore = 0;
var elencoPersone = [];
